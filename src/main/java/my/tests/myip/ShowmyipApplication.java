package my.tests.myip;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShowmyipApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShowmyipApplication.class, args);
	}
}
