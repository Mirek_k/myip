package my.tests.myip;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

@RestController
public class MainController {

	Logger logger = LoggerFactory.getLogger(MainController.class);

	public MainController() {
		super();
		Lorem lorem = LoremIpsum.getInstance();
		name = lorem.getFirstName();

		try {
			host = InetAddress.getLocalHost().toString();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		logger.info(name + " is ready to serve!!!");
	}

	String name;
	String host;

	@RequestMapping("/")
	public String index() throws IOException {
		String msg = "Greetings from " + host + " my name is " + name;
		logger.info(msg);
		return msg;
	}
}
