FROM openjdk

EXPOSE 8080

RUN useradd -m -s /bin/bash myip

ADD https://bitbucket.org/Mirek_k/myip/downloads/myip.jar /home/myip/

RUN chown -R myip /home/myip/
USER myip

WORKDIR /home/myip

CMD java -jar myip.jar